var restify = require('restify');
var ip_addr = 'localhost';
var port    =  '3000';
var Employee = require('./routes/employee');

var server = restify.createServer({
    name : "restifytest"
});


server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());


server.listen(port ,ip_addr, function(){
    console.log('%s listening at %s ', server.name , server.url);
});


// Set up our routes and start the server
server.post('/employees/createEmployee', Employee.createEmployee);
server.get('/employees', Employee.getAallEmployees);
server.del('/employees/deleteEmployee', Employee.deleteEmployee);
server.put('/employees/updateEmployee', Employee.updateEmployee);
server.get('/employees/:id', Employee.getSelectedEmployee);
