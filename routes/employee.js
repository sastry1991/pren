function EmployeeConstructor() {
    var that = this;
    var Employee = require('..//models/Employee');
    var _ = require("underscore");
    //Insert
    that.createEmployee = function (req, res, next) {
            console.log("createEmployee request " + req.url);
            console.log("req body " + req.body);
            new Employee({
                'name': req.body.name
                , 'age': req.body.age
            }).save().then(function (newEmp) {
                console.log('newEmp created!', newEmp);
            });
            res.setHeader('content-type', 'application/json');
            res.send(200);
            // next();
        }
        //Load all employees
    that.getAallEmployees = function (req, res, next) {
            console.log("getAallEmployees request " + req.url);
            new Employee().fetchAll().then(function (resData) {
                console.log(JSON.stringify(resData));
                _.each(resData.models, function (model) { //I am looping over models using underscore, you can use any loop
                    console.log(model.attributes);
                });
                res.setHeader('content-type', 'application/json');
                res.send(resData);
            });
        }
        //Load all employees
    that.deleteEmployee = function (req, res, next) {
        console.log("deleteEmployee request " + req.url);
        new Employee({
            id: req.body.id
        }).fetch().then(function (found) {
            if (found) {
                found.destroy();
                res.setHeader('content-type', 'application/json');
                res.send(200);
            }
            else {
                console.log('Emp does not exist');
            }
        });
    }
    that.updateEmployee = function (req, res, next) {
        console.log("updateEmployee request " + req.url);
        new Employee({
            id: req.body.id
        }).save({
            name: req.body.name
            , age: req.body.age
        }, {
            patch: true
        }).then(function (found) {
            if (found) {
                console.log("Balayya Udated" + JSON.stringify(found));
                res.setHeader('content-type', 'application/json');
                res.send(200);
            }
            else {
                console.log('Balayya is not accepting our request');
                res.setHeader('content-type', 'application/json');
                res.send(500);
            }
        });
    }
    that.getSelectedEmployee = function (req, res, next) {
        console.log("SelectedEmployee request " + req.url);
        new Employee({
            id: req.params.id
        }).fetch().then(function (found) {
            if (found) {
                console.log(JSON.stringify(found));
                res.setHeader('content-type', 'application/json');
                res.send(200,found);
            }
            else {
                res.setHeader('content-type', 'application/json');
                res.send(500);
            }
        });
    }
}
module.exports = new EmployeeConstructor();