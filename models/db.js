var knex = require('knex')({
  client: 'postgres',
  connection: {
    host     : 'localhost',
    user     : 'postgres',
    password  :'postgres',
    port    : 5432,
    database : 'restify',
    charset  : 'utf8'
  }
});


var db = require('bookshelf')(knex);

db.knex.schema.hasTable('users').then(function(exists) {
  if (!exists) {
    db.knex.schema.createTable('users', function (user) {
      user.increments('id').primary();
      user.string('name', 100).unique();
      user.string('pet', 100);
      user.timestamps();
    }).then(function(table) {
      console.log('Created users table');
    });
  }
});

db.knex.schema.hasTable('employee').then(function(exists) {
  if (!exists) {
    db.knex.schema.createTable('employee', function (emp) {
      emp.increments('id').primary();
      emp.string('name', 100).unique();
      emp.string('first_Name', 100);
      emp.string('last_Name', 100);
      emp.integer('age',15);
      emp.timestamps();
    }).then(function(table) {
      console.log('Created emp table');
    });
  }
});


module.exports = db;
