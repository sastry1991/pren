var db = require('./db');

var Employee = db.Model.extend({
  tableName: 'employee',
  hasTimestamps: true,
});

module.exports = Employee;
