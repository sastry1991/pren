var db = require('./db');

var User = db.Model.extend({
  tableName: 'users',
  hasTimestamps: true,
});

module.exports = User;
